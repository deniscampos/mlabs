# mlabs

[Demo do projeto](http://mlabs-denis.s3-website-sa-east-1.amazonaws.com)

## Tecnologia - Angular

Optei por utilizar o Angular pois é um dos frameworks que tenho mais experiência, apesar de trabalhar diariamente com VueJs, utilizamos ele com TypeScript o que torna muito parecido com o Angular. 

Gosto de utilizar o Angular/TypeScript porque acredito que tipar os objetos e variáveis deixa o código mais organizado e ajuda a evitar erros. Ele também permite criar componentes reutilizáveis que diminui códigos duplicados. A utilização do CLI aumenta a produtividade no dia-a-dia.

Utilizei o SASS para criar os estilos, pois agiliza muito o trabalho, deixa o CSS mais estruturado e facilita a manutenção.