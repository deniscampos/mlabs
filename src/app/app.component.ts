import { Component } from '@angular/core';
import { ConnectionService } from './services/social.service';
import { SocialEnum } from './enums/social.enum';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    private LOCAL_STORAGE_CONNECTIONS = 'social_connections';

    constructor(private socialService: ConnectionService) {
        
    }

    ngOnInit() {
    
    }

    get SocialConnections() {
        return Object.keys(SocialEnum).map(key => SocialEnum[key]);
    }
}
