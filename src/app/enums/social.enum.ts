export enum SocialEnum {
    FACEBOOK = 'facebook',
    TWITTER = 'twitter',
    INSTAGRAM = 'instagram',
    GOOGLE_BUSINESS = 'google_business',
    PINTEREST = 'pinterest',
    LINKEDIN = 'linkedin',
    YOUTUBE = 'youtube',
    WHATSAPP = 'whatsapp',
    GOOGLE_ANALYTICS = 'google_analytics',
}