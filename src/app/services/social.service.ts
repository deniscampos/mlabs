import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConnectionTypeInterface } from '../components/type-connection/types/connection-type.interface';
import { SocialEnum } from '../enums/social.enum';
import { ApiResponse } from '../models/api-response';

@Injectable({
    providedIn: 'root'
})
export class ConnectionService {
    private URL = 'https://demo2697181.mockable.io/pages';

    constructor(private http: HttpClient) {}

    pageConnections() {
        return this.http.get(this.URL) as Observable<ApiResponse>;
    }

    typeConnections() {
        return [
            {
                id: 1,
                name: 'facebook',
                icon: '<i class="fab fa-facebook-f"></i>',
                type: SocialEnum.FACEBOOK,
            } as ConnectionTypeInterface,
            {
                id: 2,
                name: 'twitter',
                icon: '<i class="fab fa-twitter"></i>',
                type: SocialEnum.TWITTER,
            } as ConnectionTypeInterface,
            {
                id: 3,
                name: 'instagram',
                icon: '<i class="fab fa-instagram"></i>',
                type: SocialEnum.INSTAGRAM,
            } as ConnectionTypeInterface,
            {
                id: 4,
                name: 'google meu negócio',
                icon: '<i class="fas fa-store"></i>',
                type: SocialEnum.GOOGLE_BUSINESS,
            } as ConnectionTypeInterface,
            {
                id: 5,
                name: 'pinterest',
                icon: '<i class="fab fa-pinterest-p"></i>',
                type: SocialEnum.PINTEREST,
            } as ConnectionTypeInterface,
            {
                id: 6,
                name: 'linkedin',
                icon: '<i class="fab fa-linkedin-in"></i>',
                type: SocialEnum.LINKEDIN,
            } as ConnectionTypeInterface,
            {
                id: 7,
                name: 'youtube',
                icon: '<i class="fab fa-youtube"></i>',
                type: SocialEnum.YOUTUBE,
            } as ConnectionTypeInterface,
            {
                id: 8,
                name: 'whatsapp',
                icon: '<i class="fab fa-whatsapp"></i>',
                type: SocialEnum.WHATSAPP,
            } as ConnectionTypeInterface,
            {
                id: 9,
                name: 'google analytics',
                icon: '<i class="fas fa-chart-line"></i>',
                type: SocialEnum.GOOGLE_ANALYTICS,
            } as ConnectionTypeInterface,
        ]
    }
}
