import { Component, OnInit } from '@angular/core';
import { ConnectionService as ConnectionService } from 'src/app/services/social.service';
import { ConnectionTypeInterface } from '../type-connection/types/connection-type.interface';
import { PageConnection } from 'src/app/models/social-connection';
import { StepInterface } from '../step/step.interface';

@Component({
    selector: 'mlabs-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    loading: boolean = false;
    showConnectionModal: boolean = false;
    pageConnectionList: PageConnection[] = [];
    pageConnectionSelected: PageConnection;
    connectionTypeList: ConnectionTypeInterface[] = [];
    connectionTypeSelected: ConnectionTypeInterface;
    LOCAL_STORAGE_CONNECTIONS = 'connections';

    constructor(private connectionService: ConnectionService) { }

    ngOnInit() {
        this.connectionTypeList = this.connectionService.typeConnections();
        this.loadConnections();
    }

    loadConnections() {
        const pageConnectionsSaved = JSON.parse(
            localStorage.getItem(this.LOCAL_STORAGE_CONNECTIONS)
        ) as ConnectionTypeInterface[];
        
        if (pageConnectionsSaved && pageConnectionsSaved.length) {
            pageConnectionsSaved.forEach(connection => {
                const ct = this.connectionTypeList.find(connectionType => connectionType.id === connection.id);
                if (!!ct)
                    ct.socialConected = connection.socialConected;
            });
        }
    }

    addConnection(connectionType: ConnectionTypeInterface) {
        this.connectionTypeSelected = connectionType;
        this.showConnectionModal = true;

        try {
            if (!this.pageConnectionList.length) {
                this.loading = true;
                
                this.connectionService
                    .pageConnections()
                    .subscribe(connections => {
                        this.pageConnectionList = connections.data;
                        this.loading = false;
                    });
                }
            
        } catch (error) {
            alert('Não foi possível carregar as páginas');
            this.loading = false;
        }
    }

    closeModal() {
        this.showConnectionModal = false;
    }


    picture(connection: PageConnection) {
        return {
            'background-image': `url(${connection.picture})`
        };
    }

    setConnectionSelected(connection: PageConnection) {
        this.pageConnectionSelected = connection;
    }

    cancelConnection() {
        this.pageConnectionSelected = null;
        this.connectionTypeSelected = null;
    }

    applyConnection() {
        this.connectionTypeSelected.socialConected = this.pageConnectionSelected;
        this.save();
        this.pageConnectionSelected = null;
        this.connectionTypeSelected = null;
    }

    save() {
        const pageConnection = this.connectionTypeList.filter(connection => !!connection.socialConected);
        localStorage.setItem(this.LOCAL_STORAGE_CONNECTIONS, JSON.stringify(pageConnection));
    }

    removeAllConnections() {
        localStorage.removeItem(this.LOCAL_STORAGE_CONNECTIONS);
        this.connectionTypeList.forEach(ctl => ctl.socialConected = null);
    }

    get connectionsFiltered() {
        if (!this.connectionTypeSelected) return [];

        return this.pageConnections
            .filter(connection => connection.channel_key === this.connectionTypeSelected.type)
            || [];
    }

    get showModal() {
        return !!this.showConnectionModal;
    }

    get pageConnections() {
        return this.pageConnectionList || [];
    }

    get connectionsType() {
        return this.connectionTypeList || [];
    }

    get okText() {
        return 'Próximo <i class="fas fa-long-arrow-alt-right"></i>';
    }

    get modalTitle() {
        if (!this.connectionTypeSelected) return;
        return `<span class="icon-circle text-${this.connectionTypeSelected.type}">
        ${this.connectionTypeSelected.icon} </span> 
        <p>Vincular página do ${this.connectionTypeSelected.name}</p>`;
    }

    get isLoading() {
        return this.loading;
    }

    get steps() {
        return [
            { name: 'Verificação', active: true } as StepInterface,
            { name: 'Página', active: true } as StepInterface,
            { name: 'Segmento', active: false } as StepInterface,
            { name: 'Concorrentes', active: false } as StepInterface,
        ]
    }
}
