import { Component, OnInit, Input } from '@angular/core';
import { StepInterface } from './step.interface';

@Component({
    selector: 'mlabs-step',
    templateUrl: './step.component.html',
    styleUrls: ['./step.component.scss']
})
export class StepComponent implements OnInit {

    @Input() steps: StepInterface[];

    constructor() { }

    ngOnInit() {
    }

    isActive(step: StepInterface) {
        if (!step.active) {
            return [];
        }

        const indexOf = this.steps.indexOf(step);
        if (this.steps[indexOf + 1].active) {
            return ['active'];
        }
    }
}
