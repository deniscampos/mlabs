export interface StepInterface {
    name: string;
    active: boolean;
}