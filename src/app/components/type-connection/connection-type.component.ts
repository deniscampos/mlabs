import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ConnectionService } from 'src/app/services/social.service';
import { ConnectionTypeInterface } from './types/connection-type.interface';

@Component({
    selector: 'mlabs-connection-type',
    templateUrl: './connection-type.component.html',
    styleUrls: ['./connection-type.component.scss']
})
export class ConnectionTypeComponent implements OnInit {

    @Input() connectionType: ConnectionTypeInterface;    
    @Output() add: EventEmitter<any> = new EventEmitter();
    
    constructor(private socialService: ConnectionService) { }

    ngOnInit() {

    }

    addConnection() {
        this.add.emit();
    } 

    get socialClass() {
        return [`bg-${this.connectionType.type}`];
    }

    get selectedIconClass() {
        return [`text-${this.connectionType.type}`];
    }

}
