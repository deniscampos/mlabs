import { SocialEnum } from 'src/app/enums/social.enum';
import { PageConnection } from 'src/app/models/social-connection';

export interface ConnectionTypeInterface {
    id: number;
    name: string;
    icon: string;
    type: SocialEnum;
    socialConected: PageConnection;
}