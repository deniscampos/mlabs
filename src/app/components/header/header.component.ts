import { Component, OnInit } from '@angular/core';
import { MenuItemInterface } from './types/menu-item.interface';

@Component({
    selector: 'mlabs-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    showMenu: boolean = false;

    constructor() { }

    ngOnInit() {
        
    }

    toggleMenu() {
        this.showMenu = !this.showMenu;
    }

    get showMenuBurger() {
        return this.showMenu;
    }

    get menuItens() {
        return [
            { 
                name: 'Dashboard', 
                icon: '<i class="fas fa-tachometer-alt"></i>',
                isNew: false,
            } as MenuItemInterface,
            { 
                name: 'Agendar post', 
                icon: '<i class="far fa-calendar-plus"></i>',
                isNew: true,
            } as MenuItemInterface,
            { 
                name: 'Calendário', 
                icon: '<i class="far fa-calendar-alt"></i>',
                isNew: false,
            } as MenuItemInterface,
            { 
                name: 'Inbox', 
                icon: '<i class="fas fa-inbox"></i>',
                isNew: false,
            } as MenuItemInterface,
            { 
                name: 'Feed', 
                icon: '<i class="far fa-newspaper"></i>',
                isNew: false,
            } as MenuItemInterface,
            { 
                name: 'Workflow', 
                icon: '<i class="fas fa-tasks"></i>',
                isNew: false,
            } as MenuItemInterface,
            { 
                name: 'Acompanhamento', 
                icon: '<i class="fas fa-chart-line"></i>',
                isNew: false,
            } as MenuItemInterface,
            { 
                name: 'Relatórios', 
                icon: '<i class="fas fa-chart-pie"></i>',
                isNew: false,
            } as MenuItemInterface,
        ];
    }
}
