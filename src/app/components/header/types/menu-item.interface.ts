export interface MenuItemInterface {
    icon: string;
    name: string;
    isNew: boolean;
}