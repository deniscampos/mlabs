import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { t } from '@angular/core/src/render3';

@Component({
    selector: 'mlabs-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

    @Input() isVisible: boolean;
    @Input() title: string;
    @Input() okText: string;
    @Input() cancelText: string;

    @Output() close: EventEmitter<any> = new EventEmitter();
    @Output() ok: EventEmitter<any> = new EventEmitter();
    @Output() cancel: EventEmitter<any> = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

    closeModal() {
        this.close.emit();
    }

    onCancel() {
        this.cancel.emit();
        this.close.emit();
    }

    onOk() {
        this.ok.emit();
        this.close.emit();
    }
    
    get showModal() {
        return this.isVisible;
    }

}
