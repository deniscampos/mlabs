import { SocialEnum } from '../enums/social.enum';

export class PageConnection {
    id: number;
    name: string;
    url: string;
    picture: string;
    channel_key: SocialEnum;
}