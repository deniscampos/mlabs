import { PageConnection } from "./social-connection";

export class ApiResponse {
    data: PageConnection[];
}